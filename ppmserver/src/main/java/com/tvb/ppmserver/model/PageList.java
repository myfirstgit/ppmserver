package com.tvb.ppmserver.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "pageList")
public class PageList {

	private List<String> url;

	public List<String> getUrl() {
		return url;
	}
	
	@XmlElement
	public void setUrl(List<String> url) {
		this.url = url;
	}
	
	public PageList() {
		super();
	}

	public PageList(List<String> url) {
		super();
		this.url = url;
	}
}