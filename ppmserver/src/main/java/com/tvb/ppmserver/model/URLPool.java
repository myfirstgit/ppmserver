package com.tvb.ppmserver.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.tvb.ppmserver.DataLoader;
import com.tvb.ppmserver.config.SystemConfiguration;

public class URLPool {
	
	private static List<String> urls = new ArrayList<String>();
	
	private static DataLoader loader;
	
	public static boolean isEmpty() {
		return urls.isEmpty();
	}
	
	public static List<String> getUrls(int threadNo) {
		
		List<String> randURLList = new ArrayList<String>();
		int urlPerThread = Integer.parseInt(SystemConfiguration.getConfig("urlPerThread", "10"));
		int pageListSize = threadNo * urlPerThread;
		
		for (int i=0; i<pageListSize; i++) {
			if (urls.size() < pageListSize) {
				urls.addAll(loader.getData());
			}
			Random randomGenerator = new Random();
			int randomNo = randomGenerator.nextInt(urls.size());
			randURLList.add(urls.get(randomNo));
			urls.remove(randomNo);
		}
		
		return randURLList;
	}

	public static void setUrls(List<String> urls) {
		URLPool.urls = urls;
	}

	public static void setLoader(DataLoader loader) {
		URLPool.loader = loader;
	}
}
