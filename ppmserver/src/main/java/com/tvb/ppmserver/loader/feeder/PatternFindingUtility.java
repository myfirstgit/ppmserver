package com.tvb.ppmserver.loader.feeder;

import java.util.ArrayList;
import java.util.List;

public class PatternFindingUtility {

	public static final List<String> getContent(String source, List<String[]> patterns) {
		List<String> sources = new ArrayList<String>();
		sources.add(source);
		return getContent(sources, patterns);
	}
	
	public static final List<String> getContent(List<String> sources, List<String[]> patterns) {
		List<String> result = new ArrayList<String>();
		if (patterns!=null) {
			if (patterns.size()>1) {
				List<String[]> nextPatterns = new ArrayList<String[]>(patterns);
				String[] pattern = nextPatterns.remove(0);
				List<String> nextSource = getContent(sources, pattern);
				for (int i = 0; i < nextSource.size(); i++) {
					result.addAll(getContent(nextSource, nextPatterns));
				}
			} else if (patterns.size()==1) {
				result.addAll(getContent(sources, patterns.get(0)));
			}
		}
		return result;
	}
	
	public static final List<String> getContent(List<String> sources, String[] pattern) {
		if (pattern!=null && pattern.length==2) {
			return getContent(sources, pattern[0], pattern[1]);
		}
		return null;
	}
	
	public static final List<String> getContent(List<String> sources, String startPattern, String endPattern) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < sources.size(); i++) {
			result.addAll(getContent(sources.get(i), startPattern, endPattern));
		}
		return result;
	}
	
	public static final List<String> getContent(String source, String[] pattern) {
		if (pattern!=null && pattern.length==2) {
			return getContent(source, pattern[0], pattern[1]);
		}
		return null;
	}
	
	public static final List<String> getContent(String source, String startPattern, String endPattern) {
		int nextPos = 0;
		List<String> result = new ArrayList<String>();
		while (nextPos>=0) {
			String[] findResult = getContent(source, startPattern, endPattern, nextPos);
			if (findResult!=null && findResult.length==2) {
				nextPos = Integer.parseInt(findResult[0]);
				if (findResult[1]!=null && findResult[1].length()>0) result.add(findResult[1]);
			} else {
				nextPos = -1;
			}
		}
		return result;
	}
	

	public static final String[] getContent(String source, String startPattern, String endPattern, int startPos) {
		if (source==null || startPattern==null || endPattern==null) return new String[] {"-1", ""};
		if (source.length()==0 || startPattern.length()==0 || endPattern.length()==0) return new String[] {"-1", ""};
		
		int pos1 = source.indexOf(startPattern, startPos);
		if (pos1>=0) {
			pos1 = pos1 + startPattern.length();
			int pos2 = source.indexOf(endPattern, pos1);
			if (pos2>=0) {
				int nextPos = pos2 + endPattern.length();
				String result = source.substring(pos1, pos2);
				return new String[] {Integer.toString(nextPos), result};
			}
		}
		return new String[] {"-1", ""};
	}

}
