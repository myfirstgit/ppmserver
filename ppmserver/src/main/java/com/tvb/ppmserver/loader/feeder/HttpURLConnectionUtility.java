package com.tvb.ppmserver.loader.feeder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpURLConnectionUtility {

	private static final String USER_AGENT = "Mozilla/5.0";
	
	public static String sendGet(String url, int retry) {
		 while(retry>=0) {
			 String result = null;
			try {
				result = sendGet(url);
			} catch (Exception e) {
				e.printStackTrace();
			}
			 if (result==null || result.length()==0) {
				 retry--;
			 } else {
				 return result;
			 }
		 }
		 return null;
	}

	
	// HTTP GET request
	private static String sendGet(String url) throws Exception {
 
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		// optional default is GET
		con.setRequestMethod("GET");
 
		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);
 
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);
 
		if (responseCode>=400) return null;
		
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine).append("\n");
		}
		in.close();
 
		//print result
		return response.toString();
 
	}
	
}
