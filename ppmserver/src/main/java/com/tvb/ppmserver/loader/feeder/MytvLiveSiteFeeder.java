package com.tvb.ppmserver.loader.feeder;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MytvLiveSiteFeeder {

	public static final String PROGRAMME_TYPES_URL = "http://mytv.tvb.com/";
	public static final String PROGRAMME_PATHS_URL = "http://mytv.tvb.com/PROGRAMME?page=PAGE";
	public static final String VIDEO_LIST_URL = "http://mytv.tvb.com/ajax/ajax_video_dropdownlist.html?programme_path=PROGRAMME_PATH";
	public static final int RETRY_COUNT = 10;
	
	public static List<String> getVideoList(List<String> programmePaths) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < programmePaths.size(); i++) {
			if (programmePaths.get(i)!=null) {
				result.addAll(getVideoList(programmePaths.get(i)));
			}
		}
		return result;		
	}
	
	public static List<String> getVideoList(String programmePath) {
		if (programmePath==null || programmePath.length()<1) return new ArrayList<String>();
		List<String[]> patternSearch = new ArrayList<String[]>();
		patternSearch.add(new String[] {"<div>", "</div>"});
		patternSearch.add(new String[] {"<a href='", "'"});
		try {
			String url = VIDEO_LIST_URL;
			url = url.replaceAll("PROGRAMME_PATH", programmePath.substring(programmePath.lastIndexOf("/")+1));
			String content = HttpURLConnectionUtility.sendGet(url, RETRY_COUNT);
			return PatternFindingUtility.getContent(content, patternSearch);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<String>();
	}

	public static List<String> getProgrammePaths(List<String> types) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < types.size(); i++) {
			if (types.get(i)!=null) {
				if (types.get(i).equals("/programme")) {
					
				} else {
					result.addAll(getProgrammePaths(types.get(i)));
				}
			}
		}
		return result;
	}
	
	public static List<String> getProgrammePaths(String type) {
		List<String> result = new ArrayList<String>();
		int page = 1;
		boolean finish = false;
		while(!finish) {
			List<String> temp = getProgrammePaths(type, page);
			if (temp!=null && temp.size()>0) {
				result.addAll(temp);
				page++;
			} else {
				finish = true;
			}
		}
		return result;
	}

	public static List<String> getProgrammePaths(String type, int page) {
		if (type==null || type.length()<1) return new ArrayList<String>();
		if (page<=0) return new ArrayList<String>();
		List<String[]> patternSearch = new ArrayList<String[]>();
		patternSearch.add(new String[] {"<div class=\"video-link\">", "<h2 class='prg-title'>"});
		patternSearch.add(new String[] {"<a href=\"", "\""});
		try {
			String url = PROGRAMME_PATHS_URL;
			url = url.replaceAll("PROGRAMME", type.substring(1));
			url = url.replaceAll("PAGE", Integer.toString(page));
			String content = HttpURLConnectionUtility.sendGet(url, RETRY_COUNT);
			return PatternFindingUtility.getContent(content, patternSearch);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<String>();
	}
	
	public static List<String> getProgrammeTypes() {
		List<String[]> patternSearch = new ArrayList<String[]>();
		patternSearch.add(new String[] {"<div id=\"sub1\" class=\"sub_menu_item hidediv\">", "</div>"});
		patternSearch.add(new String[] {"<a href=\"", "\""});
		try {
			String content = HttpURLConnectionUtility.sendGet(PROGRAMME_TYPES_URL, RETRY_COUNT);
			return PatternFindingUtility.getContent(content, patternSearch);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<String>();
	}
	
	public static void writeFile(String filePath, List<String> videoList) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < videoList.size(); i++) {
			result.append("http://mytv.tvb.com").append(videoList.get(i)).append("\n");
		}
		try {
			FileOutputStream file = new FileOutputStream(new File(filePath));
			file.write(result.toString().getBytes("utf-8"));
			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void saveVideoList(String filePath) {
		System.out.println("MytvLiveSiteCrossCheck Starting ....... " + new Date());
		List<String> videoList = getVideoList(getProgrammePaths(getProgrammeTypes()));
		writeFile(filePath, videoList);
		System.out.println("File Location: " + filePath);
		System.out.println("Record Count : " + videoList.size());
		System.out.println("MytvLiveSiteCrossCheck ....... Completed. " + new Date());
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		System.out.println(getProgrammeTypes());
//		System.out.println(getProgrammePaths("/drama"));
//		System.out.println(getVideoList("/drama/gotvrecommendation"));
//		System.out.println(getVideoList(getProgrammePaths(getProgrammeTypes())));
		if (args!=null && args.length>0) {
			saveVideoList(args[0]);
		} else {
			System.out.println("Please input the file path in arg parameter.");
		}
//		for (int i = 0; i < videoList.size(); i++) {
//			System.out.println(videoList.get(i));
//		}
		
	}

}
