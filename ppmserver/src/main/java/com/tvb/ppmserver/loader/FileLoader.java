package com.tvb.ppmserver.loader;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.tvb.ppmserver.DataLoader;

public class FileLoader implements DataLoader {
	
	private String fileName;
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<String> getData() {
		List<String> urls = null;
		File file = new File(fileName);
		try {
			urls = FileUtils.readLines(file, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return urls;
	}
}