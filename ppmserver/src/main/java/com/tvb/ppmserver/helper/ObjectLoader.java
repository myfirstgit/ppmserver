package com.tvb.ppmserver.helper;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

import com.tvb.ppmserver.config.SystemConfiguration;

public class ObjectLoader {

	public static final String CLASS_CONFIG = ".class";
	public static final String PARAMETERS_CONFIG = ".parameters.";
	
	public static Object createObject(String configPrefix) {
		String className = SystemConfiguration.getConfig(configPrefix + CLASS_CONFIG, "");
		if (className==null || className.length()==0) return null;
		Map<String, String> parameters = SystemConfiguration.getConfigMap(configPrefix + PARAMETERS_CONFIG);
		Object result;
		try {
			result = Class.forName(className).getConstructor().newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		if (parameters==null || parameters.isEmpty()) return result;
		for(String key : parameters.keySet()) {
			setPropertyValue(result, getPropertyName(key), parameters.get(key));
		}
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	private static void setPropertyValue(Object object, String propertyName, String propertyValue) {
		try {
			Class type= getPropertyClass(object, propertyName);
			if (type==null) return;
			if (type.getName().equals("int") || type.getName().equals(Integer.class.getName())) 
				PropertyUtils.setProperty(object, propertyName, new Integer(propertyValue));
			else if (type.getName().equals("short") || type.getName().equals(Short.class.getName())) 
				PropertyUtils.setProperty(object, propertyName, new Short(propertyValue));
			else if (type.getName().equals("long") || type.getName().equals(Long.class.getName()))
				PropertyUtils.setProperty(object, propertyName, new Long(propertyValue));
			else if (type.getName().equals("byte") || type.getName().equals(Byte.class.getName()))
				PropertyUtils.setProperty(object, propertyName, new Byte(propertyValue));
			else if (type.getName().equals("char") || type.getName().equals(Character.class.getName()))
				PropertyUtils.setProperty(object, propertyName, 
						new Character(propertyValue.length()>0?propertyValue.toCharArray()[0]:(char)0));
			else if (type.getName().equals("float") || type.getName().equals(Float.class.getName())) 
				PropertyUtils.setProperty(object, propertyName, new Float(propertyValue));
			else if (type.getName().equals("double") || type.getName().equals(Double.class.getName())) 
				PropertyUtils.setProperty(object, propertyName, new Double(propertyValue));
			else if (type.getName().equals("boolean") || type.getName().equals(Boolean.class.getName())) 
				PropertyUtils.setProperty(object, propertyName, new Boolean(propertyValue));
			else
				PropertyUtils.setProperty(object, propertyName, propertyValue);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("rawtypes")
	private static Class getPropertyClass(Object object, String propertyName) {
		Class type = null;
		try {
			type = PropertyUtils.getPropertyType(object, propertyName);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return type;
	}
	
	private static String getPropertyName(String key) {
		if (key==null || key.length()==0) return key;
		int index = key.lastIndexOf(".");
		if (index>=0) return key.substring(index + 1);
		return key;
	}
}
