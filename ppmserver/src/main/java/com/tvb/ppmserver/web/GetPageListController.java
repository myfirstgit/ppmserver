package com.tvb.ppmserver.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tvb.ppmserver.DataLoader;
import com.tvb.ppmserver.helper.ObjectLoader;
import com.tvb.ppmserver.model.PageList;
import com.tvb.ppmserver.model.URLPool;

@Controller
@RequestMapping("/pageList")
public class GetPageListController {

	@RequestMapping(value="{threadNo}", method = RequestMethod.GET)
	public @ResponseBody PageList getPageList(@PathVariable String threadNo) {
		
		if (URLPool.isEmpty()) {
			// Get the url list from file
			DataLoader loader = (DataLoader) ObjectLoader.createObject("dataLoader");
			//URLPool.setUrls(loader.getData());
			URLPool.setLoader(loader);
		}
		
		PageList pageList = new PageList(URLPool.getUrls(Integer.parseInt(threadNo)));
		return pageList;

	}
}