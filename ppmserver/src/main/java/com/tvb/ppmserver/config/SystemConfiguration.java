package com.tvb.ppmserver.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

public class SystemConfiguration {

	private static final String SYSTEM_PROPERTIE_FILE_NAME = "ppm.properties";
	private static final Properties systemConfig = new Properties();

	static {
		try {
//			systemConfig.load(
//					new FileInputStream(SYSTEM_PROPERTIE_FILE_NAME));
			systemConfig.load(SystemConfiguration.class.getClassLoader().getResourceAsStream(SYSTEM_PROPERTIE_FILE_NAME));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String getConfig(String name, String defaultValue) {
		return systemConfig.getProperty(name, defaultValue);
	}

	public static long getConfigLong(String name, long defaultValue) {
		String temp = systemConfig.getProperty(name, Long.toString(defaultValue));
		if (temp != null) {
			return Long.parseLong(temp);
		}
		return defaultValue;
	}

	public static double getConfigDouble(String name, double defaultValue) {
		String temp = systemConfig.getProperty(name, Double.toString(defaultValue));
		if (temp != null) {
			return Double.parseDouble(temp);
		}
		return defaultValue;
	}

	@SuppressWarnings("rawtypes")
	public static Map<String, String> getConfigMap(String prefix) {
		Map<String, String> result = new TreeMap<String, String>();
		Enumeration keys = systemConfig.keys();
		while(keys.hasMoreElements()){
			String key = (String)keys.nextElement();
			if (key.indexOf(prefix)==0) {
				result.put(key, systemConfig.getProperty(key, ""));
			}
		}
		return result;
	}

}
