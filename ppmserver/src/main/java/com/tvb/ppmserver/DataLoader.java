package com.tvb.ppmserver;

import java.util.List;

public interface DataLoader {
	public List<String> getData();
	public void setFileName(String fileName);
}
